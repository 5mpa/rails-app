class Product < ApplicationRecord
  include PgSearch::Model

  has_one_attached :picture

  pg_search_scope :search_for, against: %i(title description)

  validates :title, :price, presence: true
end
